/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.Prindi;
import java.util.List;
import javax.swing.table.AbstractTableModel;


public class PrindiTableModel extends AbstractTableModel  {
    private final String [] columnNames = {"ID","Emri","Mbiemri","NrTel","Email","Gjinia"};
    private List <Prindi> data;
    public PrindiTableModel (List<Prindi>data){
        this.data = data;
    }
     public PrindiTableModel () {
    }
    public void add(List<Prindi>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Prindi getPrindi (int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Prindi en = (Prindi)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getEmri();
            case 1:
                return en.getMbiemri();
            case 2:
                return en.getNrTel();
            case 3:
                return en.getEmail();
            case 4:
                return en.getGjinia();
                default:
                
                return null;
        }
    }
}
