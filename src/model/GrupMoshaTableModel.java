/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.GrupMosha;
import java.util.List;
import javax.swing.table.AbstractTableModel;



public class GrupMoshaTableModel extends AbstractTableModel {
    
    private final String [] columnNames = {"GrID","GrupMosha"};
 
    private List <GrupMosha> data;
    public GrupMoshaTableModel(List<GrupMosha>data){
        this.data = data;
    }
    public GrupMoshaTableModel() {
    }
    public void add(List<GrupMosha>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public GrupMosha getGrupMosha(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        GrupMosha gm = (GrupMosha)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return gm.getGrID();
            case 1:
                return gm.getGrupMosha();
            
            
            default:
                return null;
        }
    }
}
