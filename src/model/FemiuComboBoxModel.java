/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.Femiu;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;



/**
 *
 * @author Finesa
 */
public class FemiuComboBoxModel extends AbstractListModel<Femiu> implements ComboBoxModel<Femiu> {

    private List<Femiu> data;
    private Femiu selectedItem;

    public FemiuComboBoxModel(List<Femiu> data) {
        this.data = data;
    }

    public FemiuComboBoxModel() {
    }

    public void add(List<Femiu> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Femiu getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Femiu)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

}
