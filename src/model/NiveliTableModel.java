/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.Niveli;
import java.util.List;


public class NiveliTableModel {
    private final String [] columnNames = {"NiveliID","Niveli"};
    private List <Niveli> data;
    public NiveliTableModel (List<Niveli>data){
        this.data = data;
    }
     public NiveliTableModel () {
    }
    public void add(List<Niveli>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Niveli getNiveli (int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Niveli en = (Niveli)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getNiveliID();
            case 1:
                return en.getNiveli();
          
                default:
                
                return null;
        }
    }

   

    
}
