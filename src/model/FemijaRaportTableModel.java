/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.FemijaRaport;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Finesa
 */
  public class FemijaRaportTableModel  extends AbstractTableModel{
        private final String [] columnNames = {"ID","Orari" ,"Femiu","Koment","Data"};
    private List < FemijaRaport> data;
    public  FemijaRaportTableModel(List< FemijaRaport>data){
        this.data = data;
    }
    public  FemijaRaportTableModel() {
    }
    public void add(List< FemijaRaport>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public  FemijaRaport getFemijaRaport(int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
         FemijaRaport en = ( FemijaRaport)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getFRaportID();
            case 1:
                return en.getOrariID();
            case 2:
                return en.getFid();
            case 3:
                return en.getKomenti();
            case 4:
                return en.getData();
            default:
                return null;
        }
    }
 }
