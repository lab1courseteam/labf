/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import BLL.KomenteShtese;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Finesa
 */

public class KomenteShteseTableModel  extends AbstractTableModel{
        private final String [] columnNames = {"ID","Shendeti","Terapia"};
    private List <KomenteShtese> data;
    public KomenteShteseTableModel(List<KomenteShtese>data){
        this.data = data;
    }
    public KomenteShteseTableModel() {
    }
    public void add(List<KomenteShtese>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public KomenteShtese getKomenteShtese(int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        KomenteShtese en = (KomenteShtese)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getShID();
            case 1:
                return en.getShendeti();
            case 2:
                return en.getTerapia();
            
            default:
                return null;
        }
    }

    

   
}

