
package model;

import BLL.Fakulteti;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Finesa
 */

 public class FakultetiTableModel  extends AbstractTableModel{
        private final String [] columnNames = {"ID","Fakulteti"};
    private List <Fakulteti> data;
    public FakultetiTableModel(List<Fakulteti>data){
        this.data = data;
    }
    public FakultetiTableModel() {
    }
    public void add(List<Fakulteti>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Fakulteti getFakulteti(int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Fakulteti en = (Fakulteti)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getFtID();
            case 1:
                return en.getFakulteti();
               
            default:
                return null;
        }
    }
 }