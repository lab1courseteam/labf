/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.Mesuesja;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author Finesa
 */

 public class MesuesjaComboBoxModel extends AbstractListModel<Mesuesja> implements ComboBoxModel<Mesuesja> {

    private List<Mesuesja> data;
    private Mesuesja selectedItem;

    public MesuesjaComboBoxModel(List<Mesuesja> data) {
        this.data = data;
    }

    public MesuesjaComboBoxModel() {
    }

    public void add(List<Mesuesja> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Mesuesja getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Mesuesja)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

}
