/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import BLL.Grupi;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class GrupiTableModel extends AbstractTableModel {
    
    private final String [] columnNames = {"ID","Grupi" , "GrupMosha"};
 
    private List <Grupi> data;
    public GrupiTableModel(List<Grupi>data){
        this.data = data;
    }
    public GrupiTableModel() {
    }
    public void add(List<Grupi>data){
        this.data = data;
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Grupi getGrupi(int index){
        return data.get(index);
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Grupi gr = (Grupi)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return gr.getGrupiID();
            case 1:
                return gr.getGrupi();
            case 3:
                return gr.getGrupMoshaID();
            default:
                return null;
        }
    }
}