/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.Femiu;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class FemiuTableModel  extends AbstractTableModel{
        private final String [] columnNames = {"ID","Emri","Emri i Prindit","Mbiemri", "Data e lindjes" ,"Gjinia","Qyteti" ,"Shteti","Adresa"};
    private List <Femiu> data;
    public FemiuTableModel(List<Femiu>data){
        this.data = data;
    }
    public FemiuTableModel() {
    }
    public void add(List<Femiu>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Femiu getFemiu(int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Femiu en = (Femiu)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getFid();
            case 1:
                return en.getEmri();
            case 2:
                return en.getEmriPrindit();
            case 3:
                return en.getEmriPrindit();
            case 4:
                return en.getMbiemri();
            case 5:
                return en.getGjinia();
            case 6:
                return en.getDataLindjes();
            case 7:
                return en.getQyteti();

            case 8:
                return en.getShteti();
            case 9:
                return en.getAdresa();
            default:
                return null;
        }
    }

    

   
}

