/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import BLL.Niveli;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author Finesa
 */
public class NiveliComboBoxModel extends AbstractListModel<Niveli> implements ComboBoxModel<Niveli> {

    private List<Niveli> data;
    private Niveli selectedItem;

    public NiveliComboBoxModel(List<Niveli> data) {
        this.data = data;
    }

    public NiveliComboBoxModel() {
    }

    public void add(List<Niveli> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Niveli getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Niveli)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

}