/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import BLL.Enrollment;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Finesa
 */

    public class EnrollmentTableModel  extends AbstractTableModel{
        private final String [] columnNames = {"ID","Grupi","Mesuesja"};
    private List <Enrollment> data;
    public EnrollmentTableModel(List<Enrollment>data){
        this.data = data;
    }
    public EnrollmentTableModel() {
    }
    public void add(List<Enrollment>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Enrollment getEnrollment(int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Enrollment en = (Enrollment)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getEid();
            case 1:
                return en.getGrupiID();
                case 2:
                return en.getMid();
                
            default:
                return null;
        }
    }

   

   
}
