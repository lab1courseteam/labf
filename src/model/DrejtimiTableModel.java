/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.Drejtimi;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Finesa
 */
public class DrejtimiTableModel  extends AbstractTableModel{
        private final String [] columnNames = {"ID","Fakulteti" ,"Drejtimi","Niveli"};
    private List <Drejtimi> data;
    public DrejtimiTableModel(List<Drejtimi>data){
        this.data = data;
    }
    public DrejtimiTableModel() {
    }
    public void add(List<Drejtimi>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public Drejtimi getDrejtimi(int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Drejtimi en = (Drejtimi)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getDrejtimiID();
            case 1:
                return en.getDrejtimi();
            case 2:
                return en.getFakultetiID();
            case 3:
                return en.getNiveliID();
               
            default:
                return null;
        }
    }

    

   
}