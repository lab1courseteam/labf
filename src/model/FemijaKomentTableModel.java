/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.FemijaKoment;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Finesa
 */
public class FemijaKomentTableModel  extends AbstractTableModel{
        private final String [] columnNames = {"ID","Femiu","Shendeti"};
    private List <FemijaKoment> data;
    public FemijaKomentTableModel(List<FemijaKoment>data){
        this.data = data;
    }
    public FemijaKomentTableModel() {
    }
    public void add(List<FemijaKoment>data){
        this.data = data;
    }
    public int getRowCount() {
        return data.size();
    }
    public int getColumnCount() {
        return columnNames.length;
    }
    public String getColumnName(int col){
        return columnNames[col];
    }
    public void remove(int row){
        data.remove(row);
    }
    public FemijaKoment getFemijaKoment(int index){
        return data.get(index);
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        FemijaKoment en = (FemijaKoment)data.get(rowIndex);
        switch(columnIndex){
            case 0:
                return en.getFShID();
            case 1:
                return en.getFid();
            case 2:
                return en.getShID();
            
            default:
                return null;
        }
    }

    

   
}
