/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import BLL.Gjinia;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class GjiniaComboBoxModel extends AbstractListModel<Gjinia> implements ComboBoxModel<Gjinia> {

    private List<Gjinia> data;
    private Gjinia selectedItem;

    public GjiniaComboBoxModel(List<Gjinia> data) {
        this.data = data;
    }

    public GjiniaComboBoxModel() {
    }

    public void add(List<Gjinia> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Gjinia getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Gjinia)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

}

