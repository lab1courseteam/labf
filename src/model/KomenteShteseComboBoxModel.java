/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.KomenteShtese;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author Finesa
 */
public class KomenteShteseComboBoxModel extends AbstractListModel<KomenteShtese> implements ComboBoxModel<KomenteShtese> {

    private List<KomenteShtese> data;
    private KomenteShtese selectedItem;

    public KomenteShteseComboBoxModel(List<KomenteShtese> data) {
        this.data = data;
    }

    public KomenteShteseComboBoxModel() {
    }

    public void add(List<KomenteShtese> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public KomenteShtese getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(KomenteShtese)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

}