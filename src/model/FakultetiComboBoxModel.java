/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.Fakulteti;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author Finesa
 */

 public class FakultetiComboBoxModel extends AbstractListModel<Fakulteti> implements ComboBoxModel<Fakulteti> {

    private List<Fakulteti> data;
    private Fakulteti selectedItem;

    public FakultetiComboBoxModel(List<Fakulteti> data) {
        this.data = data;
    }

    public FakultetiComboBoxModel() {
    }

    public void add(List<Fakulteti> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Fakulteti getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Fakulteti)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

}
