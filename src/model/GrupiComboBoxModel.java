/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import BLL.Grupi;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author Finesa
 */

    
public class GrupiComboBoxModel extends AbstractListModel<Grupi> implements ComboBoxModel<Grupi> {

    private List<Grupi> data;
    private Grupi selectedItem;

    public GrupiComboBoxModel(List<Grupi> data) {
        this.data = data;
    }

    public GrupiComboBoxModel() {
    }

    public void add(List<Grupi> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Grupi getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem=(Grupi)anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

}
