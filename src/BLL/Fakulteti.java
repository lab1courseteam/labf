/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Fakulteti")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fakulteti.findAll", query = "SELECT f FROM Fakulteti f")
    , @NamedQuery(name = "Fakulteti.findByFtID", query = "SELECT f FROM Fakulteti f WHERE f.ftID = :ftID")
    , @NamedQuery(name = "Fakulteti.findByFakulteti", query = "SELECT f FROM Fakulteti f WHERE f.fakulteti = :fakulteti")})
public class Fakulteti implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "FtID")
    private Integer ftID;
    @Basic(optional = false)
    @Column(name = "Fakulteti")
    private String fakulteti;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fakultetiID")
    private Collection<Drejtimi> drejtimiCollection;

    public Fakulteti() {
    }

    public Fakulteti(Integer ftID) {
        this.ftID = ftID;
    }

    public Fakulteti(Integer ftID, String fakulteti) {
        this.ftID = ftID;
        this.fakulteti = fakulteti;
    }

    public Integer getFtID() {
        return ftID;
    }

    public void setFtID(Integer ftID) {
        this.ftID = ftID;
    }

    public String getFakulteti() {
        return fakulteti;
    }

    public void setFakulteti(String fakulteti) {
        this.fakulteti = fakulteti;
    }

    @XmlTransient
    public Collection<Drejtimi> getDrejtimiCollection() {
        return drejtimiCollection;
    }

    public void setDrejtimiCollection(Collection<Drejtimi> drejtimiCollection) {
        this.drejtimiCollection = drejtimiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ftID != null ? ftID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fakulteti)) {
            return false;
        }
        Fakulteti other = (Fakulteti) object;
        if ((this.ftID == null && other.ftID != null) || (this.ftID != null && !this.ftID.equals(other.ftID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Fakulteti[ ftID=" + ftID + " ]";
    }
    
}
