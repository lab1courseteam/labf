/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "FemijaRaport")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FemijaRaport.findAll", query = "SELECT f FROM FemijaRaport f")
    , @NamedQuery(name = "FemijaRaport.findByFRaportID", query = "SELECT f FROM FemijaRaport f WHERE f.fRaportID = :fRaportID")
    , @NamedQuery(name = "FemijaRaport.findByKomenti", query = "SELECT f FROM FemijaRaport f WHERE f.komenti = :komenti")
    , @NamedQuery(name = "FemijaRaport.findByData", query = "SELECT f FROM FemijaRaport f WHERE f.data = :data")})
public class FemijaRaport implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "FRaportID")
     @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer fRaportID;
    @Column(name = "Komenti")
    private String komenti;
    @Basic(optional = false)
    @Column(name = "Data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @JoinColumn(name = "FID", referencedColumnName = "FID")
    @ManyToOne(optional = false)
    private Femiu fid;
    @JoinColumn(name = "OrariID", referencedColumnName = "OrariID")
    @ManyToOne(optional = false)
    private Orari orariID;

    public FemijaRaport() {
    }

    public FemijaRaport(Integer fRaportID) {
        this.fRaportID = fRaportID;
    }

    public FemijaRaport(Integer fRaportID, Date data) {
        this.fRaportID = fRaportID;
        this.data = data;
    }

    public Integer getFRaportID() {
        return fRaportID;
    }

    public void setFRaportID(Integer fRaportID) {
        this.fRaportID = fRaportID;
    }

    public String getKomenti() {
        return komenti;
    }

    public void setKomenti(String komenti) {
        this.komenti = komenti;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Femiu getFid() {
        return fid;
    }

    public void setFid(Femiu fid) {
        this.fid = fid;
    }

    public Orari getOrariID() {
        return orariID;
    }

    public void setOrariID(Orari orariID) {
        this.orariID = orariID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fRaportID != null ? fRaportID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FemijaRaport)) {
            return false;
        }
        FemijaRaport other = (FemijaRaport) object;
        if ((this.fRaportID == null && other.fRaportID != null) || (this.fRaportID != null && !this.fRaportID.equals(other.fRaportID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.FemijaRaport[ fRaportID=" + fRaportID + " ]";
    }
    
}
