/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Kontrata")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Kontrata.findAll", query = "SELECT k FROM Kontrata k")
    , @NamedQuery(name = "Kontrata.findByKid", query = "SELECT k FROM Kontrata k WHERE k.kid = :kid")
    , @NamedQuery(name = "Kontrata.findByDataFillimit", query = "SELECT k FROM Kontrata k WHERE k.dataFillimit = :dataFillimit")
    , @NamedQuery(name = "Kontrata.findByDataMbarimit", query = "SELECT k FROM Kontrata k WHERE k.dataMbarimit = :dataMbarimit")})
public class Kontrata implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KID")
    private Integer kid;
    @Basic(optional = false)
    @Column(name = "DataFillimit")
    @Temporal(TemporalType.DATE)
    private Date dataFillimit;
    @Basic(optional = false)
    @Column(name = "DataMbarimit")
    @Temporal(TemporalType.DATE)
    private Date dataMbarimit;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "kid")
    private Collection<KontrataPrindi> kontrataPrindiCollection;
    @JoinColumn(name = "FID", referencedColumnName = "FID")
    @ManyToOne(optional = false)
    private Femiu fid;
    @JoinColumn(name = "GrupmoshaID", referencedColumnName = "GrID")
    @ManyToOne(optional = false)
    private GrupMosha grupmoshaID;
    @JoinColumn(name = "MID", referencedColumnName = "MID")
    @ManyToOne(optional = false)
    private Mesuesja mid;

    public Kontrata() {
    }

    public Kontrata(Integer kid) {
        this.kid = kid;
    }

    public Kontrata(Integer kid, Date dataFillimit, Date dataMbarimit) {
        this.kid = kid;
        this.dataFillimit = dataFillimit;
        this.dataMbarimit = dataMbarimit;
    }

    public Integer getKid() {
        return kid;
    }

    public void setKid(Integer kid) {
        this.kid = kid;
    }

    public Date getDataFillimit() {
        return dataFillimit;
    }

    public void setDataFillimit(Date dataFillimit) {
        this.dataFillimit = dataFillimit;
    }

    public Date getDataMbarimit() {
        return dataMbarimit;
    }

    public void setDataMbarimit(Date dataMbarimit) {
        this.dataMbarimit = dataMbarimit;
    }

    @XmlTransient
    public Collection<KontrataPrindi> getKontrataPrindiCollection() {
        return kontrataPrindiCollection;
    }

    public void setKontrataPrindiCollection(Collection<KontrataPrindi> kontrataPrindiCollection) {
        this.kontrataPrindiCollection = kontrataPrindiCollection;
    }

    public Femiu getFid() {
        return fid;
    }

    public void setFid(Femiu fid) {
        this.fid = fid;
    }

    public GrupMosha getGrupmoshaID() {
        return grupmoshaID;
    }

    public void setGrupmoshaID(GrupMosha grupmoshaID) {
        this.grupmoshaID = grupmoshaID;
    }

    public Mesuesja getMid() {
        return mid;
    }

    public void setMid(Mesuesja mid) {
        this.mid = mid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kid != null ? kid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kontrata)) {
            return false;
        }
        Kontrata other = (Kontrata) object;
        if ((this.kid == null && other.kid != null) || (this.kid != null && !this.kid.equals(other.kid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Kontrata[ kid=" + kid + " ]";
    }
    
}
