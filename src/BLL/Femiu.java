/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Femiu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Femiu.findAll", query = "SELECT f FROM Femiu f")
    , @NamedQuery(name = "Femiu.findByFid", query = "SELECT f FROM Femiu f WHERE f.fid = :fid")
    , @NamedQuery(name = "Femiu.findByEmri", query = "SELECT f FROM Femiu f WHERE f.emri = :emri")
    , @NamedQuery(name = "Femiu.findByEmriPrindit", query = "SELECT f FROM Femiu f WHERE f.emriPrindit = :emriPrindit")
    , @NamedQuery(name = "Femiu.findByMbiemri", query = "SELECT f FROM Femiu f WHERE f.mbiemri = :mbiemri")
    , @NamedQuery(name = "Femiu.findByDataLindjes", query = "SELECT f FROM Femiu f WHERE f.dataLindjes = :dataLindjes")
    , @NamedQuery(name = "Femiu.findByAdresa", query = "SELECT f FROM Femiu f WHERE f.adresa = :adresa")})
public class Femiu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "FID")
    private Integer fid;
    @Basic(optional = false)
    @Column(name = "Emri")
    private String emri;
    @Basic(optional = false)
    @Column(name = "EmriPrindit")
    private String emriPrindit;
    @Basic(optional = false)
    @Column(name = "Mbiemri")
    private String mbiemri;
    @Basic(optional = false)
    @Column(name = "DataLindjes")
    @Temporal(TemporalType.DATE)
    private Date dataLindjes;
    @Basic(optional = false)
    @Column(name = "Adresa")
    private String adresa;
    @JoinColumn(name = "Gjinia", referencedColumnName = "GjiniaID")
    @ManyToOne(optional = false)
    private Gjinia gjinia;
    @JoinColumn(name = "Qyteti", referencedColumnName = "QID")
    @ManyToOne(optional = false)
    private Qyteti qyteti;
    @JoinColumn(name = "Shteti", referencedColumnName = "ShID")
    @ManyToOne(optional = false)
    private Shteti shteti;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fid")
    private Collection<FemijaRaport> femijaRaportCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fid")
    private Collection<FemijaKoment> femijaKomentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fid")
    private Collection<Kontrata> kontrataCollection;

    public Femiu() {
    }

    public Femiu(Integer fid) {
        this.fid = fid;
    }

    public Femiu(Integer fid, String emri, String emriPrindit, String mbiemri, Date dataLindjes, String adresa) {
        this.fid = fid;
        this.emri = emri;
        this.emriPrindit = emriPrindit;
        this.mbiemri = mbiemri;
        this.dataLindjes = dataLindjes;
        this.adresa = adresa;
    }

    public Integer getFid() {
        return fid;
    }

    public void setFid(Integer fid) {
        this.fid = fid;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getEmriPrindit() {
        return emriPrindit;
    }

    public void setEmriPrindit(String emriPrindit) {
        this.emriPrindit = emriPrindit;
    }

    public String getMbiemri() {
        return mbiemri;
    }

    public void setMbiemri(String mbiemri) {
        this.mbiemri = mbiemri;
    }

    public Date getDataLindjes() {
        return dataLindjes;
    }

    public void setDataLindjes(Date dataLindjes) {
        this.dataLindjes = dataLindjes;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Gjinia getGjinia() {
        return gjinia;
    }

    public void setGjinia(Gjinia gjinia) {
        this.gjinia = gjinia;
    }

    public Qyteti getQyteti() {
        return qyteti;
    }

    public void setQyteti(Qyteti qyteti) {
        this.qyteti = qyteti;
    }

    public Shteti getShteti() {
        return shteti;
    }

    public void setShteti(Shteti shteti) {
        this.shteti = shteti;
    }

    @XmlTransient
    public Collection<FemijaRaport> getFemijaRaportCollection() {
        return femijaRaportCollection;
    }

    public void setFemijaRaportCollection(Collection<FemijaRaport> femijaRaportCollection) {
        this.femijaRaportCollection = femijaRaportCollection;
    }

    @XmlTransient
    public Collection<FemijaKoment> getFemijaKomentCollection() {
        return femijaKomentCollection;
    }

    public void setFemijaKomentCollection(Collection<FemijaKoment> femijaKomentCollection) {
        this.femijaKomentCollection = femijaKomentCollection;
    }

    @XmlTransient
    public Collection<Kontrata> getKontrataCollection() {
        return kontrataCollection;
    }

    public void setKontrataCollection(Collection<Kontrata> kontrataCollection) {
        this.kontrataCollection = kontrataCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fid != null ? fid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Femiu)) {
            return false;
        }
        Femiu other = (Femiu) object;
        if ((this.fid == null && other.fid != null) || (this.fid != null && !this.fid.equals(other.fid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Femiu[ fid=" + fid + " ]";
    }
    
}
