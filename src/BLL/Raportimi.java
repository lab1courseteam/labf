/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Raportimi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Raportimi.findAll", query = "SELECT r FROM Raportimi r")
    , @NamedQuery(name = "Raportimi.findByRaportimiID", query = "SELECT r FROM Raportimi r WHERE r.raportimiID = :raportimiID")
    , @NamedQuery(name = "Raportimi.findByKomenti", query = "SELECT r FROM Raportimi r WHERE r.komenti = :komenti")
    , @NamedQuery(name = "Raportimi.findByData", query = "SELECT r FROM Raportimi r WHERE r.data = :data")})
public class Raportimi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "RaportimiID")
    private Integer raportimiID;
    @Basic(optional = false)
    @Column(name = "Komenti")
    private String komenti;
    @Basic(optional = false)
    @Column(name = "Data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @JoinColumn(name = "OrariID", referencedColumnName = "OrariID")
    @ManyToOne(optional = false)
    private Orari orariID;
    @JoinColumn(name = "StatusiID", referencedColumnName = "StatusiID")
    @ManyToOne(optional = false)
    private Statusi statusiID;

    public Raportimi() {
    }

    public Raportimi(Integer raportimiID) {
        this.raportimiID = raportimiID;
    }

    public Raportimi(Integer raportimiID, String komenti, Date data) {
        this.raportimiID = raportimiID;
        this.komenti = komenti;
        this.data = data;
    }

    public Integer getRaportimiID() {
        return raportimiID;
    }

    public void setRaportimiID(Integer raportimiID) {
        this.raportimiID = raportimiID;
    }

    public String getKomenti() {
        return komenti;
    }

    public void setKomenti(String komenti) {
        this.komenti = komenti;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Orari getOrariID() {
        return orariID;
    }

    public void setOrariID(Orari orariID) {
        this.orariID = orariID;
    }

    public Statusi getStatusiID() {
        return statusiID;
    }

    public void setStatusiID(Statusi statusiID) {
        this.statusiID = statusiID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (raportimiID != null ? raportimiID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Raportimi)) {
            return false;
        }
        Raportimi other = (Raportimi) object;
        if ((this.raportimiID == null && other.raportimiID != null) || (this.raportimiID != null && !this.raportimiID.equals(other.raportimiID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Raportimi[ raportimiID=" + raportimiID + " ]";
    }
    
}
