/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Salla")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Salla.findAll", query = "SELECT s FROM Salla s")
    , @NamedQuery(name = "Salla.findBySallaID", query = "SELECT s FROM Salla s WHERE s.sallaID = :sallaID")
    , @NamedQuery(name = "Salla.findByEmertimi", query = "SELECT s FROM Salla s WHERE s.emertimi = :emertimi")})
public class Salla implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "SallaID")
    private Integer sallaID;
    @Column(name = "Emertimi")
    private String emertimi;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sallaID")
    private Collection<Orari> orariCollection;

    public Salla() {
    }

    public Salla(Integer sallaID) {
        this.sallaID = sallaID;
    }

    public Integer getSallaID() {
        return sallaID;
    }

    public void setSallaID(Integer sallaID) {
        this.sallaID = sallaID;
    }

    public String getEmertimi() {
        return emertimi;
    }

    public void setEmertimi(String emertimi) {
        this.emertimi = emertimi;
    }

    @XmlTransient
    public Collection<Orari> getOrariCollection() {
        return orariCollection;
    }

    public void setOrariCollection(Collection<Orari> orariCollection) {
        this.orariCollection = orariCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sallaID != null ? sallaID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Salla)) {
            return false;
        }
        Salla other = (Salla) object;
        if ((this.sallaID == null && other.sallaID != null) || (this.sallaID != null && !this.sallaID.equals(other.sallaID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Salla[ sallaID=" + sallaID + " ]";
    }
    
}
