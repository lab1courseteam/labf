/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Statusi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Statusi.findAll", query = "SELECT s FROM Statusi s")
    , @NamedQuery(name = "Statusi.findByStatusiID", query = "SELECT s FROM Statusi s WHERE s.statusiID = :statusiID")
    , @NamedQuery(name = "Statusi.findByEmertimi", query = "SELECT s FROM Statusi s WHERE s.emertimi = :emertimi")})
public class Statusi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "StatusiID")
    private Integer statusiID;
    @Basic(optional = false)
    @Column(name = "Emertimi")
    private String emertimi;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusiID")
    private Collection<Raportimi> raportimiCollection;

    public Statusi() {
    }

    public Statusi(Integer statusiID) {
        this.statusiID = statusiID;
    }

    public Statusi(Integer statusiID, String emertimi) {
        this.statusiID = statusiID;
        this.emertimi = emertimi;
    }

    public Integer getStatusiID() {
        return statusiID;
    }

    public void setStatusiID(Integer statusiID) {
        this.statusiID = statusiID;
    }

    public String getEmertimi() {
        return emertimi;
    }

    public void setEmertimi(String emertimi) {
        this.emertimi = emertimi;
    }

    @XmlTransient
    public Collection<Raportimi> getRaportimiCollection() {
        return raportimiCollection;
    }

    public void setRaportimiCollection(Collection<Raportimi> raportimiCollection) {
        this.raportimiCollection = raportimiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (statusiID != null ? statusiID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Statusi)) {
            return false;
        }
        Statusi other = (Statusi) object;
        if ((this.statusiID == null && other.statusiID != null) || (this.statusiID != null && !this.statusiID.equals(other.statusiID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Statusi[ statusiID=" + statusiID + " ]";
    }
    
}
