/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Niveli")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Niveli.findAll", query = "SELECT n FROM Niveli n")
    , @NamedQuery(name = "Niveli.findByNiveliID", query = "SELECT n FROM Niveli n WHERE n.niveliID = :niveliID")
    , @NamedQuery(name = "Niveli.findByNiveli", query = "SELECT n FROM Niveli n WHERE n.niveli = :niveli")})
public class Niveli implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NiveliID")
    private Integer niveliID;
    @Basic(optional = false)
    @Column(name = "Niveli")
    private String niveli;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "niveliID")
    private Collection<Drejtimi> drejtimiCollection;

    public Niveli() {
    }

    public Niveli(Integer niveliID) {
        this.niveliID = niveliID;
    }

    public Niveli(Integer niveliID, String niveli) {
        this.niveliID = niveliID;
        this.niveli = niveli;
    }

    public Integer getNiveliID() {
        return niveliID;
    }

    public void setNiveliID(Integer niveliID) {
        this.niveliID = niveliID;
    }

    public String getNiveli() {
        return niveli;
    }

    public void setNiveli(String niveli) {
        this.niveli = niveli;
    }

    @XmlTransient
    public Collection<Drejtimi> getDrejtimiCollection() {
        return drejtimiCollection;
    }

    public void setDrejtimiCollection(Collection<Drejtimi> drejtimiCollection) {
        this.drejtimiCollection = drejtimiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (niveliID != null ? niveliID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Niveli)) {
            return false;
        }
        Niveli other = (Niveli) object;
        if ((this.niveliID == null && other.niveliID != null) || (this.niveliID != null && !this.niveliID.equals(other.niveliID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Niveli[ niveliID=" + niveliID + " ]";
    }
    
}
