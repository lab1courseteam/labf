/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Qyteti")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Qyteti.findAll", query = "SELECT q FROM Qyteti q")
    , @NamedQuery(name = "Qyteti.findByQid", query = "SELECT q FROM Qyteti q WHERE q.qid = :qid")
    , @NamedQuery(name = "Qyteti.findByEmri", query = "SELECT q FROM Qyteti q WHERE q.emri = :emri")
    , @NamedQuery(name = "Qyteti.findByKodiPostar", query = "SELECT q FROM Qyteti q WHERE q.kodiPostar = :kodiPostar")})
public class Qyteti implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "QID")
    private Integer qid;
    @Basic(optional = false)
    @Column(name = "Emri")
    private String emri;
    @Basic(optional = false)
    @Column(name = "KodiPostar")
    private String kodiPostar;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "qyteti")
    private Collection<Mesuesja> mesuesjaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "qyteti")
    private Collection<Femiu> femiuCollection;

    public Qyteti() {
    }

    public Qyteti(Integer qid) {
        this.qid = qid;
    }

    public Qyteti(Integer qid, String emri, String kodiPostar) {
        this.qid = qid;
        this.emri = emri;
        this.kodiPostar = kodiPostar;
    }

    public Integer getQid() {
        return qid;
    }

    public void setQid(Integer qid) {
        this.qid = qid;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getKodiPostar() {
        return kodiPostar;
    }

    public void setKodiPostar(String kodiPostar) {
        this.kodiPostar = kodiPostar;
    }

    @XmlTransient
    public Collection<Mesuesja> getMesuesjaCollection() {
        return mesuesjaCollection;
    }

    public void setMesuesjaCollection(Collection<Mesuesja> mesuesjaCollection) {
        this.mesuesjaCollection = mesuesjaCollection;
    }

    @XmlTransient
    public Collection<Femiu> getFemiuCollection() {
        return femiuCollection;
    }

    public void setFemiuCollection(Collection<Femiu> femiuCollection) {
        this.femiuCollection = femiuCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (qid != null ? qid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Qyteti)) {
            return false;
        }
        Qyteti other = (Qyteti) object;
        if ((this.qid == null && other.qid != null) || (this.qid != null && !this.qid.equals(other.qid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Qyteti[ qid=" + qid + " ]";
    }
    
}
