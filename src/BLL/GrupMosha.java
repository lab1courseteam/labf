/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "GrupMosha")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GrupMosha.findAll", query = "SELECT g FROM GrupMosha g")
    , @NamedQuery(name = "GrupMosha.findByGrID", query = "SELECT g FROM GrupMosha g WHERE g.grID = :grID")
    , @NamedQuery(name = "GrupMosha.findByGrupMosha", query = "SELECT g FROM GrupMosha g WHERE g.grupMosha = :grupMosha")})
public class GrupMosha implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "GrID")
    private Integer grID;
    @Column(name = "GrupMosha")
    private String grupMosha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupMoshaID")
    private Collection<Grupi> grupiCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupmoshaID")
    private Collection<Kontrata> kontrataCollection;

    public GrupMosha() {
    }

    public GrupMosha(Integer grID) {
        this.grID = grID;
    }

    public Integer getGrID() {
        return grID;
    }

    public void setGrID(Integer grID) {
        this.grID = grID;
    }

    public String getGrupMosha() {
        return grupMosha;
    }

    public void setGrupMosha(String grupMosha) {
        this.grupMosha = grupMosha;
    }

    @XmlTransient
    public Collection<Grupi> getGrupiCollection() {
        return grupiCollection;
    }

    public void setGrupiCollection(Collection<Grupi> grupiCollection) {
        this.grupiCollection = grupiCollection;
    }

    @XmlTransient
    public Collection<Kontrata> getKontrataCollection() {
        return kontrataCollection;
    }

    public void setKontrataCollection(Collection<Kontrata> kontrataCollection) {
        this.kontrataCollection = kontrataCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grID != null ? grID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupMosha)) {
            return false;
        }
        GrupMosha other = (GrupMosha) object;
        if ((this.grID == null && other.grID != null) || (this.grID != null && !this.grID.equals(other.grID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.GrupMosha[ grID=" + grID + " ]";
    }
    
}
