/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "KomenteShtese")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KomenteShtese.findAll", query = "SELECT k FROM KomenteShtese k")
    , @NamedQuery(name = "KomenteShtese.findByShID", query = "SELECT k FROM KomenteShtese k WHERE k.shID = :shID")
    , @NamedQuery(name = "KomenteShtese.findByShendeti", query = "SELECT k FROM KomenteShtese k WHERE k.shendeti = :shendeti")
    , @NamedQuery(name = "KomenteShtese.findByTerapia", query = "SELECT k FROM KomenteShtese k WHERE k.terapia = :terapia")})
public class KomenteShtese implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ShID")
    private Integer shID;
    @Basic(optional = false)
    @Column(name = "Shendeti")
    private String shendeti;
    @Basic(optional = false)
    @Column(name = "Terapia")
    private String terapia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "shID")
    private Collection<FemijaKoment> femijaKomentCollection;

    public KomenteShtese() {
    }

    public KomenteShtese(Integer shID) {
        this.shID = shID;
    }

    public KomenteShtese(Integer shID, String shendeti, String terapia) {
        this.shID = shID;
        this.shendeti = shendeti;
        this.terapia = terapia;
    }

    public Integer getShID() {
        return shID;
    }

    public void setShID(Integer shID) {
        this.shID = shID;
    }

    public String getShendeti() {
        return shendeti;
    }

    public void setShendeti(String shendeti) {
        this.shendeti = shendeti;
    }

    public String getTerapia() {
        return terapia;
    }

    public void setTerapia(String terapia) {
        this.terapia = terapia;
    }

    @XmlTransient
    public Collection<FemijaKoment> getFemijaKomentCollection() {
        return femijaKomentCollection;
    }

    public void setFemijaKomentCollection(Collection<FemijaKoment> femijaKomentCollection) {
        this.femijaKomentCollection = femijaKomentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (shID != null ? shID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KomenteShtese)) {
            return false;
        }
        KomenteShtese other = (KomenteShtese) object;
        if ((this.shID == null && other.shID != null) || (this.shID != null && !this.shID.equals(other.shID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.KomenteShtese[ shID=" + shID + " ]";
    }
    
}
