/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Orari")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orari.findAll", query = "SELECT o FROM Orari o")
    , @NamedQuery(name = "Orari.findByOrariID", query = "SELECT o FROM Orari o WHERE o.orariID = :orariID")
    , @NamedQuery(name = "Orari.findByOraFillimit", query = "SELECT o FROM Orari o WHERE o.oraFillimit = :oraFillimit")
    , @NamedQuery(name = "Orari.findByOraMbarimit", query = "SELECT o FROM Orari o WHERE o.oraMbarimit = :oraMbarimit")
    , @NamedQuery(name = "Orari.findByPauza", query = "SELECT o FROM Orari o WHERE o.pauza = :pauza")
    , @NamedQuery(name = "Orari.findByDataFillimit", query = "SELECT o FROM Orari o WHERE o.dataFillimit = :dataFillimit")
    , @NamedQuery(name = "Orari.findByDataMbarimit", query = "SELECT o FROM Orari o WHERE o.dataMbarimit = :dataMbarimit")})
public class Orari implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "OrariID")
    private Integer orariID;
    @Basic(optional = false)
    @Column(name = "OraFillimit")
    private String oraFillimit;
    @Basic(optional = false)
    @Column(name = "OraMbarimit")
    private String oraMbarimit;
    @Basic(optional = false)
    @Column(name = "Pauza")
    private String pauza;
    @Basic(optional = false)
    @Column(name = "DataFillimit")
    @Temporal(TemporalType.DATE)
    private Date dataFillimit;
    @Basic(optional = false)
    @Column(name = "DataMbarimit")
    @Temporal(TemporalType.DATE)
    private Date dataMbarimit;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orariID")
    private Collection<Raportimi> raportimiCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orariID")
    private Collection<FemijaRaport> femijaRaportCollection;
    @JoinColumn(name = "EID", referencedColumnName = "EID")
    @ManyToOne(optional = false)
    private Enrollment eid;
    @JoinColumn(name = "SallaID", referencedColumnName = "SallaID")
    @ManyToOne(optional = false)
    private Salla sallaID;

    public Orari() {
    }

    public Orari(Integer orariID) {
        this.orariID = orariID;
    }

    public Orari(Integer orariID, String oraFillimit, String oraMbarimit, String pauza, Date dataFillimit, Date dataMbarimit) {
        this.orariID = orariID;
        this.oraFillimit = oraFillimit;
        this.oraMbarimit = oraMbarimit;
        this.pauza = pauza;
        this.dataFillimit = dataFillimit;
        this.dataMbarimit = dataMbarimit;
    }

    public Integer getOrariID() {
        return orariID;
    }

    public void setOrariID(Integer orariID) {
        this.orariID = orariID;
    }

    public String getOraFillimit() {
        return oraFillimit;
    }

    public void setOraFillimit(String oraFillimit) {
        this.oraFillimit = oraFillimit;
    }

    public String getOraMbarimit() {
        return oraMbarimit;
    }

    public void setOraMbarimit(String oraMbarimit) {
        this.oraMbarimit = oraMbarimit;
    }

    public String getPauza() {
        return pauza;
    }

    public void setPauza(String pauza) {
        this.pauza = pauza;
    }

    public Date getDataFillimit() {
        return dataFillimit;
    }

    public void setDataFillimit(Date dataFillimit) {
        this.dataFillimit = dataFillimit;
    }

    public Date getDataMbarimit() {
        return dataMbarimit;
    }

    public void setDataMbarimit(Date dataMbarimit) {
        this.dataMbarimit = dataMbarimit;
    }

    @XmlTransient
    public Collection<Raportimi> getRaportimiCollection() {
        return raportimiCollection;
    }

    public void setRaportimiCollection(Collection<Raportimi> raportimiCollection) {
        this.raportimiCollection = raportimiCollection;
    }

    @XmlTransient
    public Collection<FemijaRaport> getFemijaRaportCollection() {
        return femijaRaportCollection;
    }

    public void setFemijaRaportCollection(Collection<FemijaRaport> femijaRaportCollection) {
        this.femijaRaportCollection = femijaRaportCollection;
    }

    public Enrollment getEid() {
        return eid;
    }

    public void setEid(Enrollment eid) {
        this.eid = eid;
    }

    public Salla getSallaID() {
        return sallaID;
    }

    public void setSallaID(Salla sallaID) {
        this.sallaID = sallaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orariID != null ? orariID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orari)) {
            return false;
        }
        Orari other = (Orari) object;
        if ((this.orariID == null && other.orariID != null) || (this.orariID != null && !this.orariID.equals(other.orariID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Orari[ orariID=" + orariID + " ]";
    }
    
}
