/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Stafi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Stafi.findAll", query = "SELECT s FROM Stafi s")
    , @NamedQuery(name = "Stafi.findByStafiID", query = "SELECT s FROM Stafi s WHERE s.stafiID = :stafiID")
    , @NamedQuery(name = "Stafi.findByEmri", query = "SELECT s FROM Stafi s WHERE s.emri = :emri")
    , @NamedQuery(name = "Stafi.findByMbiemri", query = "SELECT s FROM Stafi s WHERE s.mbiemri = :mbiemri")
    , @NamedQuery(name = "Stafi.findByProfesioni", query = "SELECT s FROM Stafi s WHERE s.profesioni = :profesioni")
    , @NamedQuery(name = "Stafi.findByUsername", query = "SELECT s FROM Stafi s WHERE s.username = :username")
    , @NamedQuery(name = "Stafi.findByPassword", query = "SELECT s FROM Stafi s WHERE s.password = :password")})
public class Stafi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "StafiID")
    private Integer stafiID;
    @Basic(optional = false)
    @Column(name = "Emri")
    private String emri;
    @Basic(optional = false)
    @Column(name = "Mbiemri")
    private String mbiemri;
    @Basic(optional = false)
    @Column(name = "Profesioni")
    private String profesioni;
    @Basic(optional = false)
    @Column(name = "Username")
    private String username;
    @Basic(optional = false)
    @Column(name = "Password")
    private String password;
    @JoinColumn(name = "RoliID", referencedColumnName = "RoliID")
    @ManyToOne(optional = false)
    private Roli roliID;

    public Stafi() {
    }

    public Stafi(Integer stafiID) {
        this.stafiID = stafiID;
    }

    public Stafi(Integer stafiID, String emri, String mbiemri, String profesioni, String username, String password) {
        this.stafiID = stafiID;
        this.emri = emri;
        this.mbiemri = mbiemri;
        this.profesioni = profesioni;
        this.username = username;
        this.password = password;
    }

    public Integer getStafiID() {
        return stafiID;
    }

    public void setStafiID(Integer stafiID) {
        this.stafiID = stafiID;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getMbiemri() {
        return mbiemri;
    }

    public void setMbiemri(String mbiemri) {
        this.mbiemri = mbiemri;
    }

    public String getProfesioni() {
        return profesioni;
    }

    public void setProfesioni(String profesioni) {
        this.profesioni = profesioni;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Roli getRoliID() {
        return roliID;
    }

    public void setRoliID(Roli roliID) {
        this.roliID = roliID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (stafiID != null ? stafiID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stafi)) {
            return false;
        }
        Stafi other = (Stafi) object;
        if ((this.stafiID == null && other.stafiID != null) || (this.stafiID != null && !this.stafiID.equals(other.stafiID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Stafi[ stafiID=" + stafiID + " ]";
    }
    
}
