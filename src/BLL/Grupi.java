/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "Grupi")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupi.findAll", query = "SELECT g FROM Grupi g")
    , @NamedQuery(name = "Grupi.findByGrupiID", query = "SELECT g FROM Grupi g WHERE g.grupiID = :grupiID")
    , @NamedQuery(name = "Grupi.findByGrupi", query = "SELECT g FROM Grupi g WHERE g.grupi = :grupi")})
public class Grupi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "GrupiID")
    private Integer grupiID;
    @Basic(optional = false)
    @Column(name = "Grupi")
    private String grupi;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupiID")
    private Collection<Enrollment> enrollmentCollection;
    @JoinColumn(name = "GrupMoshaID", referencedColumnName = "GrID")
    @ManyToOne(optional = false)
    private GrupMosha grupMoshaID;

    public Grupi() {
    }

    public Grupi(Integer grupiID) {
        this.grupiID = grupiID;
    }

    public Grupi(Integer grupiID, String grupi) {
        this.grupiID = grupiID;
        this.grupi = grupi;
    }

    public Integer getGrupiID() {
        return grupiID;
    }

    public void setGrupiID(Integer grupiID) {
        this.grupiID = grupiID;
    }

    public String getGrupi() {
        return grupi;
    }

    public void setGrupi(String grupi) {
        this.grupi = grupi;
    }

    @XmlTransient
    public Collection<Enrollment> getEnrollmentCollection() {
        return enrollmentCollection;
    }

    public void setEnrollmentCollection(Collection<Enrollment> enrollmentCollection) {
        this.enrollmentCollection = enrollmentCollection;
    }

    public GrupMosha getGrupMoshaID() {
        return grupMoshaID;
    }

    public void setGrupMoshaID(GrupMosha grupMoshaID) {
        this.grupMoshaID = grupMoshaID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grupiID != null ? grupiID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupi)) {
            return false;
        }
        Grupi other = (Grupi) object;
        if ((this.grupiID == null && other.grupiID != null) || (this.grupiID != null && !this.grupiID.equals(other.grupiID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.Grupi[ grupiID=" + grupiID + " ]";
    }
    
}
