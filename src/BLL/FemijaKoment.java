/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Finesa
 */
@Entity
@Table(name = "FemijaKoment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FemijaKoment.findAll", query = "SELECT f FROM FemijaKoment f")
    , @NamedQuery(name = "FemijaKoment.findByFShID", query = "SELECT f FROM FemijaKoment f WHERE f.fShID = :fShID")})
public class FemijaKoment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "FShID")
    private Integer fShID;
    @JoinColumn(name = "FID", referencedColumnName = "FID")
    @ManyToOne(optional = false)
    private Femiu fid;
    @JoinColumn(name = "ShID", referencedColumnName = "ShID")
    @ManyToOne(optional = false)
    private KomenteShtese shID;

    public FemijaKoment() {
    }

    public FemijaKoment(Integer fShID) {
        this.fShID = fShID;
    }

    public Integer getFShID() {
        return fShID;
    }

    public void setFShID(Integer fShID) {
        this.fShID = fShID;
    }

    public Femiu getFid() {
        return fid;
    }

    public void setFid(Femiu fid) {
        this.fid = fid;
    }

    public KomenteShtese getShID() {
        return shID;
    }

    public void setShID(KomenteShtese shID) {
        this.shID = shID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fShID != null ? fShID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FemijaKoment)) {
            return false;
        }
        FemijaKoment other = (FemijaKoment) object;
        if ((this.fShID == null && other.fShID != null) || (this.fShID != null && !this.fShID.equals(other.fShID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.FemijaKoment[ fShID=" + fShID + " ]";
    }
    
}
