/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.FemijaKoment;
import java.util.List;

/**
 *
 * @author Finesa
 */

public interface FemijaKomentInterface {
     void create(FemijaKoment en) throws CrudFormException;
    void edit(FemijaKoment en) throws CrudFormException;
    void delete(FemijaKoment en) throws CrudFormException;
    List<FemijaKoment> findAll() throws CrudFormException;
    FemijaKoment findByID(Integer ID);
}

