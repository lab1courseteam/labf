/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Prindi;
import java.util.List;


public interface PrindiInterface {
    void create(Prindi en) throws CrudFormException;
    void edit(Prindi en) throws CrudFormException;
    void delete(Prindi en) throws CrudFormException;
    List<Prindi> findAll() throws CrudFormException;
    Prindi findByID(Integer ID);
}