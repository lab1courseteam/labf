/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Niveli;
import java.util.List;


public interface NiveliInterface {
    void create(Niveli en) throws CrudFormException;
    void edit(Niveli en) throws CrudFormException;
    void delete(Niveli en) throws CrudFormException;
    List<Niveli> findAll() throws CrudFormException;
    Niveli findByID(Integer ID);
}
