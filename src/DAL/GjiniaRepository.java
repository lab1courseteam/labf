/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Gjinia;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Finesa
 */
public class GjiniaRepository extends EntMngClass implements GjiniaInterface {

    @Override
    public void create(Gjinia en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(Gjinia en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(Gjinia en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<Gjinia> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Gjinia.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Gjinia findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Gjinia e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Gjinia)query.getSingleResult();
    }

}

 

