
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;


import BLL.Enrollment;
import java.util.List;


public interface EnrollmentInterface {
     void create(Enrollment en) throws CrudFormException;
    void edit(Enrollment en) throws CrudFormException;
    void delete(Enrollment en) throws CrudFormException;
    List<Enrollment> findAll() throws CrudFormException;
    Enrollment findByID(Integer ID);
}
