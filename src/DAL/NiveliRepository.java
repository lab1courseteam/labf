/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Niveli;
import java.util.List;
import javax.persistence.Query;

public class NiveliRepository extends EntMngClass implements NiveliInterface{
     @Override
    public void create(Niveli en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
           throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }
    @Override
   public void edit(Niveli en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
           throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }
      @Override
    public void delete(Niveli en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
           throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }
    @Override
    public List<Niveli> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Niveli.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }
     @Override
    public Niveli findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Niveli e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Niveli)query.getSingleResult();
    }
}
