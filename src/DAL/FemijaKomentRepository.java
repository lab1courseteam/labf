
package DAL;


 

import BLL.FemijaKoment;
import java.util.List;
import javax.persistence.Query;

public class FemijaKomentRepository extends EntMngClass implements FemijaKomentInterface {

    @Override
    public void create(FemijaKoment en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(FemijaKoment en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(FemijaKoment en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<FemijaKoment> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("FemijaKoment.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public FemijaKoment findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM FemijaKoment e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (FemijaKoment)query.getSingleResult();
    }

}

 

