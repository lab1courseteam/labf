/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Mesuesja;
import java.util.List;

/**
 *
 * @author Finesa
 */

 public interface MesuesjaInterface {
     void create(Mesuesja en) throws CrudFormException;
    void edit(Mesuesja en) throws CrudFormException;
    void delete(Mesuesja en) throws CrudFormException;
    List<Mesuesja> findAll() throws CrudFormException;
    Mesuesja findByID(Integer ID);
}