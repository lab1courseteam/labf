/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Enrollment;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Finesa
 */
public class EnrollmentRepository extends EntMngClass implements EnrollmentInterface {

    @Override
    public void create(Enrollment en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(Enrollment en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(Enrollment en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<Enrollment> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Enrollment.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Enrollment findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Enrollment e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Enrollment)query.getSingleResult();
    }
}
