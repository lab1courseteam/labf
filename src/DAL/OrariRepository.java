/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Orari;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Finesa
 */
public class OrariRepository extends EntMngClass implements OrariInterface {    
public void create(Orari sh) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(sh);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n"  +e.getMessage());
        }
    }

    public void edit(Orari sh) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(sh);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Orari sh) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(sh);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n"+  e.getMessage());
        }
    }

    public List<Orari> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Orari.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Orari findByID(Integer ID) {
       Query query = em.createQuery("SELECT e FROM Orari e WHERE e.ID = :id");
        query.setParameter("id", ID);
        return (Orari)query.getSingleResult();
    }
    }
