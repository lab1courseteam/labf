/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import java.util.List;
import BLL.FemijaRaport;
/**
 *
 * @author Finesa
 */

     public interface FemijaRaportInterface {
     void create(FemijaRaport en) throws CrudFormException;
    void edit(FemijaRaport en) throws CrudFormException;
    void delete(FemijaRaport en) throws CrudFormException;
    List<FemijaRaport> findAll() throws CrudFormException;
    FemijaRaport findByID(Integer ID);
}

