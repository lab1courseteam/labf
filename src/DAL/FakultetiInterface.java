/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Fakulteti;
import java.util.List;

/**
 *
 * @author Finesa
 */

 public interface FakultetiInterface {
     void create(Fakulteti en) throws CrudFormException;
    void edit(Fakulteti en) throws CrudFormException;
    void delete(Fakulteti en) throws CrudFormException;
    List<Fakulteti> findAll() throws CrudFormException;
    Fakulteti findByID(Integer ID);
}
