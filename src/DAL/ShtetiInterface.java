/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Shteti;
import java.util.List;

/**
 *
 * @author Finesa
 */

 public interface ShtetiInterface {
     void create(Shteti en) throws CrudFormException;
    void edit(Shteti en) throws CrudFormException;
    void delete(Shteti en) throws CrudFormException;
    List<Shteti> findAll() throws CrudFormException;
    Shteti findByID(Integer ID);
}