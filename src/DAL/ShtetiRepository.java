/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Shteti;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Finesa
 */

public class ShtetiRepository extends EntMngClass implements ShtetiInterface{
    @Override
    public void create(Shteti en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(Shteti en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(Shteti en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<Shteti> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Shteti.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Shteti findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Shteti e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Shteti)query.getSingleResult();
    }

}
