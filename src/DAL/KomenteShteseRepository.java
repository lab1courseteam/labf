/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.KomenteShtese;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author elina
 */
public class KomenteShteseRepository extends EntMngClass implements KomenteShteseInterface {    
public void create(KomenteShtese sh) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(sh);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n"  +e.getMessage());
        }
    }

    public void edit(KomenteShtese sh) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(sh);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(KomenteShtese sh) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(sh);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n"+  e.getMessage());
        }
    }

    public List<KomenteShtese> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("KomenteShtese.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public KomenteShtese findByID(Integer ID) {
       Query query = em.createQuery("SELECT e FROM KomenteShtese e WHERE e.ID = :id");
        query.setParameter("id", ID);
        return (KomenteShtese)query.getSingleResult();
    }
    }