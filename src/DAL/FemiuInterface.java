/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Femiu;
import java.util.List;

public interface FemiuInterface {
     void create(Femiu en) throws CrudFormException;
    void edit(Femiu en) throws CrudFormException;
    void delete(Femiu en) throws CrudFormException;
    List<Femiu> findAll() throws CrudFormException;
    Femiu findByID(Integer ID);
}
