/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Grupi;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author elina
 */
public class GrupiRepository extends EntMngClass implements GrupiInterface {

    public void create(Grupi gr) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(gr);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void edit(Grupi gr) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(gr);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public void delete(Grupi gr) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(gr);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    public List<Grupi> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Grupi.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    public Grupi findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Grupi e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Grupi)query.getSingleResult();
    }
}
