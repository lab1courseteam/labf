/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.FemijaRaport;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Finesa
 */
 public class FemijaRaportRepository extends EntMngClass implements FemijaRaportInterface {

    @Override
    public void create(FemijaRaport en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(FemijaRaport en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(FemijaRaport en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<FemijaRaport> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("FemijaRaport.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public FemijaRaport findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM FemijaRaport e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (FemijaRaport)query.getSingleResult();
    }
}

