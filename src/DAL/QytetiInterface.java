/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Qyteti;
import java.util.List;

/**
 *
 * @author Finesa
 */

 public interface QytetiInterface {
     void create(Qyteti en) throws CrudFormException;
    void edit(Qyteti en) throws CrudFormException;
    void delete(Qyteti en) throws CrudFormException;
    List<Qyteti> findAll() throws CrudFormException;
    Qyteti findByID(Integer ID);
}