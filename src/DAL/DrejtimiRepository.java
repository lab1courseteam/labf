/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Drejtimi;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Finesa
 */
public class DrejtimiRepository extends EntMngClass implements DrejtimiInterface {

    @Override
    public void create(Drejtimi en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(Drejtimi en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(Drejtimi en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<Drejtimi> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Drejtimi.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Drejtimi findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Drejtimi e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Drejtimi)query.getSingleResult();
    }
}
