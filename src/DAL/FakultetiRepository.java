/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Fakulteti;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Finesa
 */

 public class FakultetiRepository extends EntMngClass implements FakultetiInterface {

    @Override
    public void create(Fakulteti en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(Fakulteti en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(Fakulteti en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<Fakulteti> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Fakulteti.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Fakulteti findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Fakulteti e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Fakulteti)query.getSingleResult();
    }
}

