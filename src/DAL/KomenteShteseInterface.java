/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.KomenteShtese;
import java.util.List;

public interface KomenteShteseInterface {
     void create(KomenteShtese sh) throws CrudFormException;
    void edit(KomenteShtese sh) throws CrudFormException;
    void delete(KomenteShtese sh) throws CrudFormException;
    List<KomenteShtese> findAll() throws CrudFormException;
    KomenteShtese findByID(Integer ID);

}
