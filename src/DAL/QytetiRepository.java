/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Qyteti;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Finesa
 */

  public class QytetiRepository extends EntMngClass implements QytetiInterface{
    @Override
    public void create(Qyteti en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(Qyteti en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(Qyteti en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<Qyteti> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Qyteti.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Qyteti findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Qyteti e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Qyteti)query.getSingleResult();
    }

}

