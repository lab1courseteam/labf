/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Mesuesja;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author Finesa
 */

public class MesuesjaRepository extends EntMngClass implements MesuesjaInterface {

    @Override
    public void create(Mesuesja en) throws CrudFormException {
        try {
            em.getTransaction().begin();
            em.persist(en);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(Mesuesja en) throws CrudFormException{
		try {
			em.getTransaction().begin();
			em.merge(en);
			em.getTransaction().commit();
		 } catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(Mesuesja en) throws CrudFormException{
		try{	
			em.getTransaction().begin();
			em.remove(en);
			em.getTransaction().commit();
		}catch (Exception e) {
            throw new CrudFormException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<Mesuesja> findAll() throws CrudFormException {
        try {
            return em.createNamedQuery("Mesuesja.findAll").getResultList();
        } catch (Exception e) {
            throw new CrudFormException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public Mesuesja findByID(Integer id) {
        Query query = em.createQuery("SELECT e FROM Mesuesja e WHERE e.ID = :id");
        query.setParameter("id", id);
        return (Mesuesja)query.getSingleResult();
    }

}

 


