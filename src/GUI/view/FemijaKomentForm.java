
package GUI.view;

import BLL.FemijaKoment;
import BLL.Femiu;
import BLL.KomenteShtese;
import DAL.CrudFormException;
import DAL.FemijaKomentInterface;
import DAL.FemijaKomentRepository;
import DAL.FemiuRepository;
import DAL.KomenteShteseRepository;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import model.FemijaKomentTableModel;
import model.FemiuComboBoxModel;
import model.KomenteShteseComboBoxModel;


public class FemijaKomentForm extends javax.swing.JInternalFrame {
FemijaKomentInterface fi = new FemijaKomentRepository();
 FemijaKomentTableModel ftm= new FemijaKomentTableModel();
 FemiuRepository fr = new FemiuRepository();
 FemiuComboBoxModel fcmb = new FemiuComboBoxModel ();
 KomenteShteseRepository kr = new KomenteShteseRepository();
 KomenteShteseComboBoxModel kcmb = new KomenteShteseComboBoxModel();
 
    public FemijaKomentForm() throws CrudFormException {
        initComponents();
        loadTable();
        tabelaSelectedIndexChange();
        loadComboBox();
    }
    public void loadTable(){
      try{
            List<FemijaKoment> lista = fi.findAll();
            ftm.add(lista);
            tabela.setModel(ftm);
            ftm.fireTableDataChanged();
      } catch(CrudFormException ene){
        JOptionPane.showMessageDialog(this, ene.getMessage());
      }
    }
    
     public void loadComboBox()  {
    try {
        List<Femiu> lista1 = fr.findAll();
        fcmb.add(lista1);
        ComboBox1.setModel(fcmb);
        ComboBox1.repaint();
        List<KomenteShtese> lista2 = kr.findAll();
        kcmb.add(lista2);
        ComboBox2.setModel(kcmb);
        ComboBox2.repaint();
    } catch (CrudFormException ex) {
        Logger.getLogger(FemijaKomentForm.class.getName()).log(Level.SEVERE, null, ex);
    }

    }
    
    private void tabelaSelectedIndexChange() {
		final ListSelectionModel rowSM = tabela.getSelectionModel();
		rowSM.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    FemijaKoment en = ftm.getFemijaKoment(selectedIndex);

                    txtID.setText(en.getFShID() + "");
                   fcmb.setSelectedItem(en.getFid());
                   ComboBox1.repaint();
                   kcmb.setSelectedItem(en.getFid());
                   ComboBox2.repaint();

                }
            }
        });
    }
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabela = new javax.swing.JTable();
        txtID = new javax.swing.JTextField();
        ComboBox1 = new javax.swing.JComboBox();
        ComboBox2 = new javax.swing.JComboBox();
        savebtn = new javax.swing.JButton();
        editbtn = new javax.swing.JButton();
        clearbtn = new javax.swing.JButton();
        deletebtn = new javax.swing.JButton();

        jLabel1.setText("ID:");

        jLabel2.setText("Femiu:");

        jLabel3.setText("Shendeti :");

        tabela.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabela);

        ComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBox1ActionPerformed(evt);
            }
        });

        ComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        savebtn.setText("Save");
        savebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savebtnActionPerformed(evt);
            }
        });

        editbtn.setText("Edit");
        editbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editbtnActionPerformed(evt);
            }
        });

        clearbtn.setText("Clear");
        clearbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearbtnActionPerformed(evt);
            }
        });

        deletebtn.setText("Delete");
        deletebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletebtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addGap(27, 27, 27)
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(ComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(savebtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(editbtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(clearbtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deletebtn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(ComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(ComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(savebtn)
                    .addComponent(editbtn)
                    .addComponent(clearbtn)
                    .addComponent(deletebtn))
                .addGap(28, 28, 28)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void savebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savebtnActionPerformed
        // TODO add your handling code here:
         int row = tabela.getSelectedRow();
        if (!txtID.getText().equals("")) {
            if (row == -1) {
                FemijaKoment en = new FemijaKoment();
                en.setFShID(Integer.parseInt(txtID.getText()));
                en.setFid((Femiu) fcmb.getSelectedItem());
                en.setShID((KomenteShtese) kcmb.getSelectedItem());
                try {
                    fi.create(en);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }
                
            } else {
                FemijaKoment en = ftm.getFemijaKoment(row);
                en.setFid((Femiu) fcmb.getSelectedItem());
                en.setShID((KomenteShtese) kcmb.getSelectedItem());
                
                try {
                    fi.edit(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(FemijaKomentForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            clearField();
            loadTable();
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }//GEN-LAST:event_savebtnActionPerformed

    private void editbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editbtnActionPerformed
        // TODO add your handling code here:
            int row = tabela.getSelectedRow();
        if (!txtID.getText().equals("")) {
            if (row == -1) {
                FemijaKoment en = new FemijaKoment();
                en.setFShID(Integer.parseInt(txtID.getText()));
                en.setFid((Femiu) fcmb.getSelectedItem());
                en.setShID((KomenteShtese) kcmb.getSelectedItem());
                
                try {
                    fi.create(en);
                } catch (CrudFormException ene) {
                    JOptionPane.showMessageDialog(this, ene.getMessage());
                }
                
            } else {
                FemijaKoment en = ftm.getFemijaKoment(row);
                en.setFid((Femiu) fcmb.getSelectedItem());
                en.setShID((KomenteShtese) kcmb.getSelectedItem());
                
                
                try {
                    fi.edit(en);
                } catch (CrudFormException ex) {
                    Logger.getLogger(FemijaKomentForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            clearField();
            loadTable();
        } else {
            JOptionPane.showMessageDialog(this, "Ju lutem plotesoni fushat obligative (me shenjen *)!");

        }
    }//GEN-LAST:event_editbtnActionPerformed

    private void clearbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearbtnActionPerformed
        // TODO add your handling code here:
        clearField();
    }//GEN-LAST:event_clearbtnActionPerformed

    private void deletebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletebtnActionPerformed
        // TODO add your handling code here:
         int row = tabela.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i = 
			JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja", 
			JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                FemijaKoment en = ftm.getFemijaKoment(row);
                try{
                    fi.delete(en);
                }
                catch(CrudFormException ex){
                    Logger.getLogger(FemijaKomentForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                clearField();
                loadTable();
            } else {
                clearField();
            }
        }else{
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }
    }//GEN-LAST:event_deletebtnActionPerformed

    private void ComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboBox1ActionPerformed

    private void clearField() {
        txtID.setText("");
        ComboBox1.setSelectedIndex(-1);
       ComboBox1.repaint();
        ComboBox2.setSelectedIndex(-1);
       ComboBox2.repaint();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox ComboBox1;
    private javax.swing.JComboBox ComboBox2;
    private javax.swing.JButton clearbtn;
    private javax.swing.JButton deletebtn;
    private javax.swing.JButton editbtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton savebtn;
    private javax.swing.JTable tabela;
    private javax.swing.JTextField txtID;
    // End of variables declaration//GEN-END:variables
}
